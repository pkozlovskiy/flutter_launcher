import 'package:flutter/material.dart';
import 'package:flutter_launcher/features/settings/settings_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'features/main/main_page.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final sharedPreferences = await SharedPreferences.getInstance();

  runApp(MyApp(sharedPreferences));
}

class MyApp extends StatelessWidget {
  final sharedPreferences;

  MyApp(this.sharedPreferences);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(),
      home: MainPage(),
      routes: {
        SettingsPage.route: (context) => SettingsPage(),
      },
      // routes: ,
    );
  }
}
