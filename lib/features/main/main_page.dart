import 'package:flutter/material.dart';
import 'package:flutter_launcher/features/settings/settings_page.dart';

class MainPage extends StatefulWidget {
  MainPage({Key key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          _buildBackGround(),
          _buildMain(),
          Align(
            alignment: Alignment.bottomRight,
            child: IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {
                //TODO open settigs
                Navigator.pushNamed(context, SettingsPage.route);
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildBackGround() {
    //TODO load bg asset from settings
    //
    // Image.asset('{$path}');
    return Container(color: Colors.white);
  }

  Widget _buildMain() {
    // FutureBuilder(
    //   future: LauncherAssist.getAllApps(),
    //   builder: (context, snapshot) {
    //     if (snapshot.hasData) {
    //       dynamic apps = snapshot.data;
    //     }
    //     return Center(
    //       child: CircularProgressIndicator(),
    //     );
    //   },
    // );
    //TODO build main interface
    return Container();
  }
}
