import 'package:flutter/material.dart';

class SettingsPage extends StatefulWidget {
  static String route = 'settings';

  SettingsPage({Key key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
      ),
      body: ListView(
        children: [
          ListTile(
            title: Text('Background'),
            trailing: Icon(Icons.navigate_next),
            onTap: () {
              //TODO pick background file path and save in settings
            },
          ),
        ],
      ),
    );
  }
}
